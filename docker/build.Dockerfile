FROM registry.gitlab.com/triastanto/laravel-10-kbs/base:latest

RUN git clone -b main --depth=1 https://$CI_JOB_TOKEN@gitlab.com/triastanto/laravel-10-kbs.git .
COPY --chown=john:john .env .
RUN composer install && \
  php artisan key:generate && \
  php artisan config:cache